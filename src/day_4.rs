/*
--- Day 4: The Ideal Stocking Stuffer ---

Santa needs help mining some AdventCoins (very similar to bitcoins) to use as gifts for all the economically forward-thinking little girls and boys.
To do this, he needs to find MD5 hashes which, in hexadecimal, start with at least five zeroes. The input to the MD5 hash is some secret key (your puzzle input, given below) followed by a number in decimal. To mine AdventCoins, you must find Santa the lowest positive number (no leading zeroes: 1, 2, 3, ...) that produces such a hash.

For example:

    If your secret key is abcdef, the answer is 609043, because the MD5 hash of abcdef609043 starts with five zeroes (000001dbbfa...), and it is the lowest such number to do so.
    If your secret key is pqrstuv, the lowest number it combines with to make an MD5 hash starting with five zeroes is 1048970; that is, the MD5 hash of pqrstuv1048970 looks like 000006136ef....

Your puzzle answer was 254575.

--- Part Two ---

Now find one that starts with six zeroes.

Your puzzle answer was 1038736.
*/

extern crate crypto;

use self::crypto::md5::Md5;
use self::crypto::digest::Digest;

#[allow(dead_code)]
fn part_1_imperative(input :&str) -> u32 {
    let mut h = Md5::new();
    for x in 1..u32::max_value() {
        let i = format!("{}{}", input, x);
        h.input_str(&i);
        if h.result_str().starts_with("00000") {
            return x
        }
        h.reset();
    }
    panic!("Didn't find any number");
}

#[allow(dead_code)]
fn part_1_functional(input :&str) -> usize {
    (0..u32::max_value())
        .map(|i| format!("{}{}", input, i))
        .map(|s| {let mut h = Md5::new(); h.input_str(&s); h.result_str()})
        .enumerate()
        .filter(|h| h.1.starts_with("00000"))
        .next()
        .expect("Didn't find any number").0
}

#[allow(dead_code)]
fn part_2_imperative(input :&str) -> u32 {
    let mut h = Md5::new();
    for x in 1..u32::max_value() {
        let i = format!("{}{}", input, x);
        h.input_str(&i);
        if h.result_str().starts_with("000000") {
            return x;
        }
        h.reset();
    }
    panic!("Didn't find any number");
}

#[allow(dead_code)]
fn part_2_functional(input :&str) -> usize {
    (0..u32::max_value())
        .map(|i| format!("{}{}", input, i))
        .map(|s| {let mut h = Md5::new(); h.input_str(&s); h.result_str()})
        .enumerate()
        .filter(|h| h.1.starts_with("000000"))
        .next()
        .expect("Didn't find any number").0
}

#[test]
fn puzzle() {
    let input = "bgvyzdsv";
    assert_eq!(part_1_imperative(input), 254575);
    assert_eq!(part_1_functional(input), 254575);
    assert_eq!(part_2_imperative(input), 1038736);
    assert_eq!(part_2_functional(input), 1038736);
}
