/*
--- Day 10: Elves Look, Elves Say ---

Today, the Elves are playing a game called [look-and-say](https://en.wikipedia.org/wiki/Look-and-say_sequence). They take turns making sequences by reading aloud the previous sequence and using that reading as the next sequence. For example, 211 is read as "one two, two ones", which becomes 1221 (1 2, 2 1s).
Look-and-say sequences are generated iteratively, using the previous value as input for the next step. For each step, take the previous value, and replace each run of digits (like 111) with the number of digits (3) followed by the digit itself (1).

For example:

    1 becomes 11 (1 copy of digit 1).
    11 becomes 21 (2 copies of digit 1).
    21 becomes 1211 (one 2 followed by one 1).
    1211 becomes 111221 (one 1, one 2, and two 1s).
    111221 becomes 312211 (three 1s, two 2s, and one 1).

Starting with the digits in your puzzle input, apply this process 40 times. What is the length of the result?

Your puzzle answer was 360154.

--- Part Two ---

Neat, right? You might also enjoy hearing [John Conway talking about this sequence](https://www.youtube.com/watch?v=ea7lJkEhytA) (that's Conway of Conway's Game of Life fame).
Now, starting again with the digits in your puzzle input, apply this process 50 times. What is the length of the new result?

Your puzzle answer was 5103798.
*/

extern crate regex;

use self::regex::Regex;

#[allow(dead_code)]
fn part_1_imperative(input :&str, n :usize) -> usize {
    let mut s = input.clone().to_string();
    for _ in 0..n {
        let mut build = String::with_capacity((s.len() as f32 * 1.4) as usize);
        let mut current = s.chars().next().unwrap();
        let mut count = 0;
        for c in s.chars() {
            if c != current {
                // We can also use char::from_u32_unchecked(count + 31)
                build.push_str(format!("{}{}", count, current).as_str());
                current = c;
                count = 0;
            }
            count += 1;
        }
        build.push_str(format!("{}{}", count, current).as_str());
        s = build;
    }
    s.len()
}

#[allow(dead_code)]
fn part_1_imperative_slow(input :&str, n :usize) -> usize {
    // This is a slow version using regex and is not executed during tests.
    // Just keeping it as an example.
    // Note that you can't have a sequence > 3
    let mut s = input.clone().to_string();
    let re = Regex::new(r"(1+|2+|3+)").unwrap();
    for _ in 0..n {
        let mut build = String::new();
        for rep in re.captures_iter(s.as_str()) {
            let m = rep.at(1).unwrap();
            build.push_str(format!("{}{}", m.len(), m.chars().next().unwrap()).as_str());
        }
        s = build;
    }
    s.len()
}

#[allow(dead_code)]
fn part_1_functional(input :&str, n :usize)  -> usize {
    match n {
        0 => input.len(),
        _ => part_1_functional(input.chars()
            .chain("-".chars())
            // Not really happy of this scan, maybe another way to do this ?
            .scan((0, ' '), |d, c| {
                let mut r = (d.0, d.1, false);
                if c != d.1 {
                    r.2 = true;
                    *d = (0, c);
                }
                d.0 += 1;
                Some(r)
            })
            .filter(|r| r.0 > 0 && r.2)
            .map(|r| format!("{}{}", r.0, r.1))
            .collect::<String>().as_str(), n - 1)
    }
}

#[test]
fn puzzle() {
    // Previously: 3 -> 13 -> 1113 -> 3113 -> 132113
    let input = "1113122113";
    assert_eq!(part_1_imperative(input, 40), 360154);
    assert_eq!(part_1_functional(input, 40), 360154);
    //assert_eq!(part_1_imperative_slow(input, 40), 360154);
    assert_eq!(part_1_imperative(input, 50), 5103798);
    assert_eq!(part_1_functional(input, 50), 5103798);
}
