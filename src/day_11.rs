/*
--- Day 11: Corporate Policy ---

Santa's previous password expired, and he needs help choosing a new one.
To help him remember his new password after the old one expires, Santa has devised a method of coming up with a password based on the previous one. Corporate policy dictates that passwords must be exactly eight lowercase letters (for security reasons), so he finds his new password by incrementing his old password string repeatedly until it is valid.
Incrementing is just like counting with numbers: xx, xy, xz, ya, yb, and so on. Increase the rightmost letter one step; if it was z, it wraps around to a, and repeat with the next letter to the left until one doesn't wrap around.

Unfortunately for Santa, a new Security-Elf recently started, and he has imposed some additional password requirements:

- Passwords must include one increasing straight of at least three letters, like abc, bcd, cde, and so on, up to xyz. They cannot skip letters; abd doesn't count.
- Passwords may not contain the letters i, o, or l, as these letters can be mistaken for other characters and are therefore confusing.
- Passwords must contain at least two different, non-overlapping pairs of letters, like aa, bb, or zz.

For example:

- hijklmmn meets the first requirement (because it contains the straight hij) but fails the second requirement requirement (because it contains i and l).
- abbceffg meets the third requirement (because it repeats bb and ff) but fails the first requirement.
- abbcegjk fails the third requirement, because it only has one double letter (bb).
- The next password after abcdefgh is abcdffaa.
- The next password after ghijklmn is ghjaabcc, because you eventually skip all the passwords that start with ghi..., since i is not allowed.

Given Santa's current password (your puzzle input), what should his next password be?

Your puzzle answer was hxbxxyzz.

--- Part Two ---

Santa's password expired again. What's the next one?

Your puzzle answer was hxcaabcc.
*/

#[allow(dead_code)]
fn part_1_imperative(input :&str) -> String {
    let mut psw = input.to_string();
    loop {
        // Build next iteration
        let mut next = String::new();
        let mut done = false;
        for c in psw.chars().rev() {
            if !done {
                next.push(match c {
                    'z' => 'a',
                    _ => {done = true; (c as u8 + 1) as char}
                });
            } else {
                next.push(c);
            }
        }
        psw = next.chars().rev().collect::<String>();
        // Check that we have two different pairs
        let mut prev = psw.chars().next().unwrap();
        let mut pairs = String::new();
        for c in psw.chars().skip(1) {
            if c == prev && !pairs.contains(c) {
                pairs.push(c);
            }
            prev = c;
        }
        if pairs.len() < 2 {
            continue
        }
        // Check that we have a straight sequence
        for s in psw.chars().zip(psw.chars().skip(1)).zip(psw.chars().skip(2)) {
            if ((s.0).0 as u8 == (s.0).1 as u8 - 1) && ((s.0).1 as u8 == s.1 as u8 - 1) {
                return psw.clone();
            }
        }
    }

}

#[allow(dead_code)]
fn part_1_functional(input :&str) -> String {
    (0..).scan(input.to_string(), |psw, _| {
            *psw = psw.chars().rev().scan(true, |r, c| {
                match *r {
                    false => Some(c),
                    true => match c {
                        'z' => Some('a'),
                        _ => {*r = false; Some((c as u8 + 1) as char)}
                    }
                }
            }).collect::<String>().chars().rev().collect::<String>();
            Some(psw.clone())})
        .filter(|psw| !(psw.contains('i') || psw.contains('o') || psw.contains('l')))
        .filter(|psw| psw.chars().zip(psw.chars().skip(1))
            .filter(|x| x.0 == x.1)
            .scan(String::new(), |chars, pair| {
                let mut r = false;
                if !chars.contains(pair.0) {
                    chars.push(pair.0);
                    r = true;
                }
                Some(r)})
            .filter(|x| *x)
            .count() >= 2)
        .filter(|psw|
            psw.chars().zip(psw.chars().skip(1)).zip(psw.chars().skip(2))
            .filter(|x| ((x.0).0 as u8 == (x.0).1 as u8 - 1) && ((x.0).1 as u8 == x.1 as u8 - 1))
            .count() > 0)
        .next().expect("Cannot find next password")
}

#[test]
fn puzzle() {
    assert_eq!(part_1_imperative("hxbxwxoa"), "hxbxxyzz");
    assert_eq!(part_1_functional("hxbxwxoa"), "hxbxxyzz");
    assert_eq!(part_1_imperative("hxbxxyzz"), "hxcaabcc");
    assert_eq!(part_1_functional("hxbxxyzz"), "hxcaabcc");
}
