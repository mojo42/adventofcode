/*
--- Day 9: All in a Single Night ---

Every year, Santa manages to deliver all of his presents in a single night.
This year, however, he has some new locations to visit; his elves have provided him the distances between every pair of locations. He can start and end at any two (different) locations he wants, but he must visit each location exactly once. What is the shortest distance he can travel to achieve this?

For example, given the following distances:

London to Dublin = 464
London to Belfast = 518
Dublin to Belfast = 141

The possible routes are therefore:

Dublin -> London -> Belfast = 982
London -> Dublin -> Belfast = 605
London -> Belfast -> Dublin = 659
Dublin -> Belfast -> London = 659
Belfast -> Dublin -> London = 605
Belfast -> London -> Dublin = 982

The shortest of these is London -> Dublin -> Belfast = 605, and so the answer is 605 in this example.
What is the distance of the shortest route?

Your puzzle answer was 207.

--- Part Two ---

The next year, just to show off, Santa decides to take the route with the longest distance instead.
He can still start and end at any two (different) locations he wants, and he still must visit each location exactly once.
For example, given the distances above, the longest route would be 982 via (for example) Dublin -> London -> Belfast.
What is the distance of the longest route?

Your puzzle answer was 804.
*/

extern crate regex;
extern crate permutohedron;

use self::regex::Regex;
use std::collections::HashMap;
use self::permutohedron::Heap;

#[allow(dead_code)]
fn part_1_imperative(input :&str) -> usize {
    let mut dists :HashMap<String, usize> = HashMap::new();
    let mut cities :HashMap<String, bool> = HashMap::new();
    let re = Regex::new(r"^([:alpha:]+) to ([:alpha:]+) = (\d+)").unwrap();
    for l in input.lines() {
        let c = re.captures(l).unwrap();
        let d = (c.at(1).unwrap().to_string(),
                 c.at(2).unwrap().to_string(),
                 c.at(3).unwrap().parse::<usize>().unwrap());
        dists.insert(d.0.clone() + &d.1, d.2);
        dists.insert(d.1.clone() + &d.0, d.2);
        dists.insert(d.0.clone(), 0);
        dists.insert(d.1.clone(), 0);
        cities.insert(d.0, true);
        cities.insert(d.1, true);
    }

    // I can have used permutohedron crate to iterate over combinaisons but
    // it was funnier to make it's own recursive version.
    // Any way, if you are looking to a permutation algorith take a look at permutohedron crate
    // or take a look here:
    // https://docs.python.org/3/library/itertools.html?highlight=itertools#itertools.permutations
    fn find(start: &String,
            cities :&HashMap<String, bool>,
            dists :&HashMap<String, usize>) -> usize {
        let mut min :usize = usize::max_value();
        for (c, _) in cities.iter() {
            let mut ci = cities.clone();
            ci.remove(c);
            let tot = match cities.len() {
                1 => 0,
                _ => find(c, &ci, &dists)
            } + *dists.get(&{start.clone() + c}).unwrap();
            if tot < min {
                min = tot;
            }
        }
        min
    }
    find(&"".to_string(), &cities, &dists)
}

#[allow(dead_code)]
fn part_1_functional(input :&str) -> usize {
    Heap::new(&mut
        input.lines()
            .map(|l| {
                let c = Regex::new(r"^([:alpha:]+) to ([:alpha:]+).*").unwrap().captures(l).unwrap();
                (c.at(1).unwrap().to_string(),
                 c.at(2).unwrap().to_string())
            })
            .fold(HashMap::new(), |mut c, l| {
                c.insert(l.0, 0);
                c.insert(l.1, 0);
                c})
            .iter().map(|c| c.0).collect::<Vec<_>>())
    .scan(input.lines()
        .map(|l| {
            let c = Regex::new(r"^([:alpha:]+) to ([:alpha:]+) = (\d+)").unwrap().captures(l).unwrap();
            (c.at(1).unwrap().to_string(),
             c.at(2).unwrap().to_string(),
             c.at(3).unwrap().parse::<usize>().unwrap())
        })
        .fold(HashMap::new(), |mut c, l| {
            c.insert(l.0.clone() + &l.1, l.2);
            c.insert(l.1.clone() + &l.0, l.2);
            c}),
        |distances, cities|
            Some((0..cities.len()-1)
                .map(|i| distances.get(&{cities[i].clone() + &cities[i+1]}).unwrap())
                .fold(0, |total, s| total + s))
    )
    .fold(usize::max_value(), |mut min, d| {if d < min {min = d;} min})
}

#[allow(dead_code)]
fn part_2_imperative(input :&str) -> usize {
    let mut dists :HashMap<String, usize> = HashMap::new();
    let mut cities :HashMap<String, bool> = HashMap::new();
    let re = Regex::new(r"^([:alpha:]+) to ([:alpha:]+) = (\d+)").unwrap();
    for l in input.lines() {
        let c = re.captures(l).unwrap();
        let d = (c.at(1).unwrap().to_string(),
                 c.at(2).unwrap().to_string(),
                 c.at(3).unwrap().parse::<usize>().unwrap());
        dists.insert(d.0.clone() + &d.1, d.2);
        dists.insert(d.1.clone() + &d.0, d.2);
        dists.insert(d.0.clone(), 0);
        dists.insert(d.1.clone(), 0);
        cities.insert(d.0, true);
        cities.insert(d.1, true);
    }

    fn find(start: &String,
            cities :&HashMap<String, bool>,
            dists :&HashMap<String, usize>) -> usize {
        let mut max :usize = 0;
        for (c, _) in cities.iter() {
            let mut ci = cities.clone();
            ci.remove(c);
            let tot = match cities.len() {
                1 => 0,
                _ => find(c, &ci, &dists)
            } + *dists.get(&{start.clone() + c}).unwrap();
            if tot > max {
                max = tot;
            }
        }
        max
    }
    find(&"".to_string(), &cities, &dists)
}

#[allow(dead_code)]
fn part_2_functional(input :&str) -> usize {
    Heap::new(&mut
        input.lines()
            .map(|l| {
                let c = Regex::new(r"^([:alpha:]+) to ([:alpha:]+).*").unwrap().captures(l).unwrap();
                (c.at(1).unwrap().to_string(),
                 c.at(2).unwrap().to_string())
            })
            .fold(HashMap::new(), |mut c, l| {
                c.insert(l.0, 0);
                c.insert(l.1, 0);
                c})
            .iter().map(|c| c.0).collect::<Vec<_>>())
    .scan(input.lines()
        .map(|l| {
            let c = Regex::new(r"^([:alpha:]+) to ([:alpha:]+) = (\d+)").unwrap().captures(l).unwrap();
            (c.at(1).unwrap().to_string(),
             c.at(2).unwrap().to_string(),
             c.at(3).unwrap().parse::<usize>().unwrap())
        })
        .fold(HashMap::new(), |mut c, l| {
            c.insert(l.0.clone() + &l.1, l.2);
            c.insert(l.1.clone() + &l.0, l.2);
            c}),
        |distances, cities|
            Some((0..cities.len()-1)
                .map(|i| distances.get(&{cities[i].clone() + &cities[i+1]}).unwrap())
                .fold(0, |total, s| total + s))
    )
    .fold(0, |mut max, d| {if d > max {max = d;} max})
}

#[test]
fn puzzle() {
    let input = "Faerun to Norrath = 129
Faerun to Tristram = 58
Faerun to AlphaCentauri = 13
Faerun to Arbre = 24
Faerun to Snowdin = 60
Faerun to Tambi = 71
Faerun to Straylight = 67
Norrath to Tristram = 142
Norrath to AlphaCentauri = 15
Norrath to Arbre = 135
Norrath to Snowdin = 75
Norrath to Tambi = 82
Norrath to Straylight = 54
Tristram to AlphaCentauri = 118
Tristram to Arbre = 122
Tristram to Snowdin = 103
Tristram to Tambi = 49
Tristram to Straylight = 97
AlphaCentauri to Arbre = 116
AlphaCentauri to Snowdin = 12
AlphaCentauri to Tambi = 18
AlphaCentauri to Straylight = 91
Arbre to Snowdin = 129
Arbre to Tambi = 53
Arbre to Straylight = 40
Snowdin to Tambi = 15
Snowdin to Straylight = 99
Tambi to Straylight = 70";
    assert_eq!(part_1_imperative(input), 207);
    assert_eq!(part_1_functional(input), 207);
    assert_eq!(part_2_imperative(input), 804);
    assert_eq!(part_2_functional(input), 804);
}
