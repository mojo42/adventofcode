# Advent of code

Some exercises from [adventofcode.com](http://adventofcode.com) using
[Rust](http://rust-lang.org/).

Goals:
- Provide an imperative and functional solution
- Do not aim to be memory or cpu optimal
- Be minimalist

Comments and propositions to make the code cleaner are welcome :)

Resources:
- [rustbyexample.com](http://rustbyexample.com/fn/hof.html) provides a nice
  example of Higher Order Functions (HOF).
- [Iterators documentation](http://doc.rust-lang.org/core/iter/trait.Iterator.html)
   which is a good source way to test/understand Iterators.

## Warning: sources contains *spoilers* about the results :)
